git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init  (inicia o git no repositório)

git status   (mostra os status dos arquivos do repositório, se foram chamados pelo add, se tem 
		algum para receber o commit, se todos já receberam o commit, ou se algum granc não recebeu um merge)

git add arquivo/diretório   (chama algum arquivo específico)
git add --all = git add .   (chama todos os arquivos no diretório)

git commit -m “Primeiro commit”   (salvar nova versão)

-------------------------------------------------------

git log   (mostra todas as versões salvas de todos os arquivos no diretório)
git log arquivo   (mostra as versões salvas de um arquivo específico)
git reflog   (mostra todas as versões salvas de todos os arquivos de maneira resumida)

-------------------------------------------------------

git show   (mostra todo o conteúdo das commits)
git show <commit>   (mostra todo o conteúdo de uma commit específica)

-------------------------------------------------------

git diff   (mostra as diferenças entre as commits)
git diff <commit1> <commit2>   (mostra as diferenças entre commits específicas)

-------------------------------------------------------

git reset --hard <commit>   (desfaz as alterações nos arquivos)

-------------------------------------------------------

git branch   (lista os branchs)
git branch -r    (lista todas as branchs remotas)
git branch -a   (lista todas as branchs locais e remotas)
git branch -d <branch_name>    (deleta uma branch que já foi adicionada à linha do tempo principal)
git branch -D <branch_name>    (força um delete do branch)
git branch -m <nome_novo>   (troca o nome de uma branch específica)
git branch -m <nome_antigo> <nome_novo>   (troca o nome de qualquer branch)

-------------------------------------------------------

git checkout <branch_name>   (abre um branch)
git checkout -b <branch_name>    (cria e abre um branch)

-------------------------------------------------------

git merge <branch_name>   (adiciona o branch à linha do tempo principal)

-------------------------------------------------------

git clone   (clona um repositório para a pasta)
git pull   (adiciona as alterações de uma branch remota no repositório local)
git push   (adiciona os commits locais para uma branch)

-------------------------------------------------------

git remote -v   (mostra os servidores remotos)
git remote add origin <url>   (adiciona um servidor remoto aos arquivos locais de uma url)
git remote <url> origin   (troca a url origem para uma nova)

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


